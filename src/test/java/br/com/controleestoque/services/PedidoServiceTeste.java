package br.com.controleestoque.services;

import br.com.controleestoque.enums.TipoPagamentoEnum;
import br.com.controleestoque.models.*;
import br.com.controleestoque.models.dtos.PedidoClienteRespostaDTO;
import br.com.controleestoque.models.dtos.PedidoRespostaDTO;
import br.com.controleestoque.repositories.PedidoRepository;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;

import java.time.LocalDate;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

@SpringBootTest
public class PedidoServiceTeste {

    @MockBean
    private PedidoRepository pedidoRepository;

    @MockBean
    private ClienteService clienteService;

    @MockBean
    private ProdutoService produtoService;

    @MockBean
    private EstoqueService estoqueService;

    @MockBean
    private CupomService cupomService;

    @Autowired
    private PedidoService pedidoService;

    Pedido pedido;
    Cliente cliente;
    Cupom cupom;
    Estoque estoque;
    Produto produto;
    List<Produto> produtos;
    PedidoRespostaDTO pedidoRespostaDTO;
    PedidoClienteRespostaDTO pedidoClienteRespostaDTO;

    @BeforeEach
    public void setUp(){
        cliente = new Cliente();
        cliente.setCpf("55043272023");
        cliente.setEmail("cliente@gmail.com");
        cliente.setId(1);

        cupom = new Cupom();
        cupom.setDataValidade(LocalDate.now());
        cupom.setNomeCupom("PROMO10");
        cupom.setPorcentagemDesconto(10);
        cupom.setId(1);

        produto = new Produto();
        produto.setId(1);
        produto.setDescricaoProduto("Sandalia");
        produto.setValorUnitario(50.00);

        produtos = new ArrayList<>();
        produtos.add(produto);

        estoque = new Estoque(10 , produto);

        pedido = new Pedido();
        pedido.setCliente(cliente);
        pedido.setDataDaCompra(LocalDate.now());
        pedido.setProdutos(produtos);
        pedido.setCupom(cupom);
        pedido.setId(1);
        pedido.setTipoPagamento(TipoPagamentoEnum.CARTAOCREDITO);
        pedido.setValorTotal(45.00);

        pedidoRespostaDTO = new PedidoRespostaDTO(pedido.getValorTotal(),pedido.getProdutos().size(), pedido.getTipoPagamento(),
                pedido.getDataDaCompra());

        pedidoClienteRespostaDTO = new PedidoClienteRespostaDTO(cliente.getNome(),pedido.getDataDaCompra(),
                produto.getDescricaoProduto(), pedido.getValorTotal());
    }

    @Test
    public void testarListarValorTotalDoDiaPorData(){
        Iterable<Pedido> pedidos = Arrays.asList(pedido);
        Mockito.when(pedidoRepository.findAllByDataDaCompra(pedido.getDataDaCompra())).thenReturn(pedidos);

        Iterable<PedidoRespostaDTO> pedidoRespostaDTOS = pedidoService.listarValorTotalDoDia(pedido.getDataDaCompra());

        Assertions.assertEquals(((List)pedidos).size(), ((List)pedidoRespostaDTOS).size());
    }

    @Test
    public void testarListarValorTotalDeCompras(){
        Iterable<Pedido> pedidos = Arrays.asList(pedido);
        Mockito.when(pedidoRepository.findAll()).thenReturn(pedidos);

        Iterable<PedidoRespostaDTO> pedidoRespostaDTOS = pedidoService.listarValorTotal();

        Assertions.assertEquals(((List)pedidos).size(), ((List)pedidoRespostaDTOS).size());
    }

    @Test
    public void testarRegistrarCompraClienteNaoEncontrado(){
        Mockito.when(clienteService.buscarClientePorID(Mockito.anyInt())).thenThrow(RuntimeException.class);

        Assertions.assertThrows(RuntimeException.class, () -> {pedidoService.registrarCompra(pedido);});
    }

    @Test
    public void testarRegistrarCompraProdutoNaoEnviado(){
        boolean existeProduto = false;
        Mockito.when(produtoService.existeProduto(Mockito.anyInt())).thenReturn(existeProduto);

        Assertions.assertThrows(RuntimeException.class, () -> {pedidoService.registrarCompra(pedido);});
    }

    @Test
    public void testarRegistrarCompraSemEstoque(){
        long qtdeEstoque = 0;
        boolean existeProduto = true;
        Mockito.when(clienteService.buscarClientePorID(Mockito.anyInt())).thenReturn(cliente);
        Mockito.when(produtoService.existeProduto(Mockito.anyInt())).thenReturn(existeProduto);
        Mockito.when(estoqueService.consultarEstoque(Mockito.anyInt())).thenReturn(qtdeEstoque);

        Assertions.assertThrows(RuntimeException.class, () -> {pedidoService.registrarCompra(pedido);});
    }

    @Test
    public void testarRegistrarCompraComEstoque(){
        boolean existeProduto = true;
        Mockito.when(clienteService.buscarClientePorID(Mockito.anyInt())).thenReturn(cliente);
        Mockito.when(produtoService.existeProduto(Mockito.anyInt())).thenReturn(existeProduto);
        Mockito.when(estoqueService.consultarEstoque(Mockito.anyInt())).thenReturn(estoque.getQuantidadeEstoque());

        pedidoService.registrarCompra(pedido);
        Mockito.verify(estoqueService, Mockito.times(1)).reduzirEstoque(Mockito.anyList(), Mockito.anyInt());
    }

    @Test
    public void testarRegistrarCompraComCupomInvalido(){
        boolean existeProduto = true;
        Mockito.when(clienteService.buscarClientePorID(Mockito.anyInt())).thenReturn(cliente);
        Mockito.when(produtoService.existeProduto(Mockito.anyInt())).thenReturn(existeProduto);
        Mockito.when(cupomService.buscarPorID(Mockito.anyInt())).thenThrow(RuntimeException.class);

        pedido.setCupom(null);
        Assertions.assertThrows(RuntimeException.class, () -> {pedidoService.registrarCompra(pedido);});

    }

    @Test
    public void testarRegistrarCompraComCupom(){
        Mockito.when(clienteService.buscarClientePorID(Mockito.anyInt())).thenReturn(cliente);
        Mockito.when(produtoService.existeProduto(Mockito.anyInt())).thenReturn(true);
        Mockito.when(pedidoRepository.save(Mockito.any(Pedido.class))).thenReturn(pedido);
        Mockito.when(estoqueService.consultarEstoque(Mockito.anyInt())).thenReturn(estoque.getQuantidadeEstoque());
        Mockito.when(produtoService.buscarTodosPorID(Mockito.anyList())).thenReturn(produtos);

        PedidoRespostaDTO pedidoObjetoDTO = pedidoService.registrarCompra(pedido);

        Assertions.assertEquals(LocalDate.now(), pedidoObjetoDTO.getDataDaCompra());
        Assertions.assertEquals(produto, pedido.getProdutos().get(0));
        Assertions.assertEquals(pedidoObjetoDTO.getValorTotalDaCompra(), pedido.getValorTotal());
    }

    @Test
    public void testarRegistrarCompraSemCupom(){
        Mockito.when(clienteService.buscarClientePorID(Mockito.anyInt())).thenReturn(cliente);
        Mockito.when(produtoService.existeProduto(Mockito.anyInt())).thenReturn(true);
        Mockito.when(pedidoRepository.save(Mockito.any(Pedido.class))).thenReturn(pedido);
        Mockito.when(estoqueService.consultarEstoque(Mockito.anyInt())).thenReturn(estoque.getQuantidadeEstoque());
        Mockito.when(produtoService.buscarTodosPorID(Mockito.anyList())).thenReturn(produtos);

        Pedido pedidoObjeto = new Pedido();
        pedidoObjeto.setCliente(cliente);
        pedidoObjeto.setDataDaCompra(LocalDate.now());
        pedidoObjeto.setProdutos(produtos);
        pedidoObjeto.setCupom(null);
        pedidoObjeto.setId(1);
        pedidoObjeto.setTipoPagamento(TipoPagamentoEnum.CARTAOCREDITO);
        pedidoObjeto.setValorTotal(50.00);

        PedidoRespostaDTO pedidoObjetoDTO = pedidoService.registrarCompra(pedidoObjeto);

        Assertions.assertEquals(LocalDate.now(), pedidoObjetoDTO.getDataDaCompra());
        Assertions.assertEquals(produto, pedidoObjeto.getProdutos().get(0));
        Assertions.assertEquals(pedidoObjetoDTO.getValorTotalDaCompra(), pedidoObjeto.getValorTotal());
    }

    @Test
    public void testarListarPedidosPorTipoPagamento(){
        Iterable<Pedido> pedidos = Arrays.asList(pedido);
        Mockito.when(pedidoRepository.findAllByTipoPagamento(pedido.getTipoPagamento())).thenReturn(pedidos);

        Iterable<PedidoRespostaDTO> pedidoRespostaDTOS = pedidoService.listarPorTipoPagamento(pedido.getTipoPagamento());

        Assertions.assertEquals(((List)pedidos).size(), ((List)pedidoRespostaDTOS).size());
    }

    @Test
    public void testarListarPedidosPorCliente(){
        Iterable<Pedido> pedidos = Arrays.asList(pedido);
        Mockito.when(clienteService.buscarClientePorID(pedido.getCliente().getId())).thenReturn(cliente);
        Mockito.when(pedidoRepository.findAllByCliente(pedido.getCliente())).thenReturn(pedidos);

        Iterable<PedidoClienteRespostaDTO> pedidoClienteRespostaDTOS = pedidoService.listarPedidoPorCliente(
                pedido.getCliente().getId());

        Assertions.assertEquals(((List)pedidos).size(), ((List)pedidoClienteRespostaDTOS).size());
    }
}
