package br.com.controleestoque.services;

import br.com.controleestoque.models.Cliente;
import br.com.controleestoque.repositories.ClienteRepository;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.Mockito;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;

@SpringBootTest
public class ClienteServiceTeste {

    @MockBean
    private ClienteRepository clienteRepository;

    @Autowired
    private ClienteService clienteService;

    Cliente cliente;

    @BeforeEach
    public void setUp(){
        cliente = new Cliente();
        cliente.setNome("Julio");
        cliente.setCpf("52957395037");
        cliente.setEmail("julioteste@teste.com");


    }


    @Test
    public  void testarCadastrarCliente(){

        Cliente clienteTeste = new Cliente();
        clienteTeste.setNome("Garsare");
        clienteTeste.setCpf("35602801090");
        clienteTeste.setEmail("garsare@teste.com");

        Mockito.when(clienteRepository.save(clienteTeste)).then(leadLamb -> {
            // Simula o preenchimento do id como se fosse o banco de dados;
            clienteTeste.setId(1);
            return clienteTeste;
        });

        Cliente clienteObjeto = clienteService.cadastrarCliente(clienteTeste);

        Assertions.assertEquals(1, clienteTeste.getId());
        Assertions.assertEquals(clienteObjeto, clienteTeste);
    }

    @Test
    public void testarBuscarClientePorEmail(){

        Mockito.when(clienteRepository.findByEmail(Mockito.anyString())).thenReturn(cliente);
        Cliente clientePorEmail = clienteService.buscarClientePorEmail("garsare@teste.com");
        Assertions.assertEquals(clientePorEmail, cliente);
    }

    @Test
    public void testarBuscarClientePorID(){

        Mockito.when(clienteRepository.findById(Mockito.anyInt())).thenReturn(java.util.Optional.ofNullable(cliente));
        Cliente clientePorID = clienteService.buscarClientePorID(1);
        Assertions.assertEquals(clientePorID, cliente);
    }

    @Test
    public void testarBuscarClientePorIDInexistente(){

        Mockito.when(clienteRepository.findById(Mockito.anyInt())).thenReturn(null);
        Assertions.assertThrows(RuntimeException.class, () -> {clienteService.buscarClientePorID(2);});
    }

}
