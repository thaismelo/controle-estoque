package br.com.controleestoque.services;


import br.com.controleestoque.models.Cliente;
import br.com.controleestoque.models.Estoque;
import br.com.controleestoque.models.Produto;
import br.com.controleestoque.repositories.EstoqueRepository;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

@SpringBootTest
public class EstoqueServiceTeste {


    @MockBean
    EstoqueRepository estoqueRepository;

    @Autowired
    EstoqueService estoqueService;

    @MockBean
    ProdutoService produtoService;

    @MockBean
    Estoque estoqueMock;


    Produto produto;
    Produto produto2;
    Estoque estoque;
    Estoque estoque2;

    @BeforeEach
    public void setUp(){
        produto = new Produto();
        produto.setId(1);
        produto.setDescricaoProduto("Fone de ouvido");
        produto.setValorUnitario(500);

        estoque = new Estoque();
        estoque.setId(1);
        estoque.setProduto(produto);
        estoque.setQuantidadeEstoque(5);

        produto2 = new Produto();
        produto2.setId(2);
        produto2.setDescricaoProduto("Mouse");
        produto2.setValorUnitario(100);

        estoque2 = new Estoque();
        estoque2.setId(2);
        estoque2.setProduto(produto2);
        estoque2.setQuantidadeEstoque(3);

    }



    @Test
    public void testarCadastrarEstoque(){
        Estoque estoqueCadastrar = new Estoque();
        estoqueCadastrar.setProduto(produto);
        estoqueCadastrar.setQuantidadeEstoque(5);

        Mockito.when(estoqueRepository.save(estoqueCadastrar)).then(leadLamb -> {
            // Simula o preenchimento do id como se fosse o banco de dados;
            estoqueCadastrar.setId(2);
            return estoqueCadastrar;
        });

        Estoque estoqueObjeto = estoqueService.cadastrarEstoque(estoqueCadastrar);

        Assertions.assertEquals(2, estoqueCadastrar.getId());
        Assertions.assertEquals(estoqueObjeto, estoqueCadastrar);
    }

    @Test
    public void testarConsultarEstoque(){
        Mockito.when(produtoService.buscarPorId(Mockito.anyInt())).thenReturn(produto);



        Mockito.when(estoqueRepository.findByProduto(produto)).thenReturn(Optional.ofNullable(estoque));

        long estoquePorId = estoqueService.consultarEstoque(1);
        Assertions.assertEquals(estoquePorId, estoque.getQuantidadeEstoque());
    }

    @Test
    public void testarConsultarEstoqueInexistente(){
        Mockito.when(produtoService.buscarPorId(Mockito.anyInt())).thenReturn(produto);
        Mockito.when(estoqueRepository.findByProduto(produto)).thenReturn(Optional.ofNullable(null));
        Assertions.assertThrows(RuntimeException.class, () -> {estoqueService.consultarEstoque(1);});
    }

    @Test
    public void testarReduzirEstoque(){
        List<Integer> produtos = new ArrayList<>();
        produtos.add(produto.getId());
        produtos.add(produto2.getId());


        Mockito.when(produtoService.buscarPorId(1)).thenReturn(produto);
        Mockito.when(produtoService.buscarPorId(2)).thenReturn(produto2);
        Mockito.when(estoqueRepository.findByProduto(produto)).thenReturn(Optional.ofNullable(estoque));
        Mockito.when(estoqueRepository.findByProduto(produto2)).thenReturn(Optional.ofNullable(estoque2));

        Mockito.when(estoqueRepository.save(estoque)).then(leadLamb -> {
            // Simula o preenchimento do id como se fosse o banco de dados;
            estoque.setId(1);
            return estoque;
        });

        Mockito.when(estoqueRepository.save(estoque2)).then(leadLamb -> {
            // Simula o preenchimento do id como se fosse o banco de dados;
            estoque2.setId(2);
            return estoque2;
        });

        //Mockito.when(estoque.reduzirEstoque(Mockito.anyInt())).thenReturn()
        estoqueService.reduzirEstoque(produtos, 1);

        Mockito.verify(estoqueRepository, Mockito.times(2)).save(Mockito.any());




    }

}
