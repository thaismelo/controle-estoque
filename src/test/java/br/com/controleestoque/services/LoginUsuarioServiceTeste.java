package br.com.controleestoque.services;

import br.com.controleestoque.models.Usuario;
import br.com.controleestoque.repositories.UsuarioRepository;
import br.com.controleestoque.security.LoginUsuario;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.Mockito;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.security.core.userdetails.UsernameNotFoundException;

import java.util.Optional;

@SpringBootTest
public class LoginUsuarioServiceTeste {

    @MockBean
    private UsuarioRepository usuarioRepository;

    @Autowired
    private LoginUsuarioService loginUsuarioService;

    Usuario usuario;
    LoginUsuario loginUsuario;

    @BeforeEach
    public void setUp(){
        usuario =  new Usuario();
        usuario.setSenha("alegria");
        usuario.setEmail("ana@gmail.com");
        usuario.setId(1);

        loginUsuario = new LoginUsuario(usuario.getId(), usuario.getEmail(), usuario.getSenha());
    }

    @Test
    public void testarBuscarUsuarioNaoCadastrado(){
        Usuario usuarioObjeto =  null;
        Mockito.when(usuarioRepository.findByEmail(Mockito.anyString())).thenReturn(usuarioObjeto);

        Assertions.assertThrows(UsernameNotFoundException.class, () -> {loginUsuarioService.loadUserByUsername(Mockito.anyString());});
    }

    @Test
    public void testarBuscarUsuarioCadastrado(){
        Mockito.when(usuarioRepository.findByEmail(Mockito.anyString())).thenReturn(usuario);

        Assertions.assertSame(loginUsuarioService.loadUserByUsername(Mockito.anyString()).getUsername(), loginUsuario.getUsername());
    }
}
