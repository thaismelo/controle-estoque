package br.com.controleestoque.services;

import br.com.controleestoque.models.Usuario;
import br.com.controleestoque.repositories.PedidoRepository;
import br.com.controleestoque.repositories.UsuarioRepository;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.Mockito;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;

@SpringBootTest
public class UsuarioServiceTeste {

    @MockBean
    private UsuarioRepository usuarioRepository;

    @Autowired
    private UsuarioService usuarioService;

    Usuario usuario;

    @BeforeEach
    public void setUp(){
        usuario =  new Usuario();
        usuario.setSenha("alegria");
        usuario.setEmail("ana@gmail.com");
        usuario.setId(1);
    }

    @Test
    public void testarSalvarUsuario(){
        Mockito.when(usuarioRepository.save(usuario)).thenReturn(usuario);

        Usuario usuarioObjeto = usuarioService.salvarUsuario(usuario);

        Assertions.assertEquals(usuario, usuarioObjeto);
    }
}
