package br.com.controleestoque.services;

import br.com.controleestoque.models.Cliente;
import br.com.controleestoque.models.Cupom;
import br.com.controleestoque.repositories.CupomRepository;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.internal.util.collections.Iterables;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;

import java.text.DateFormat;
import java.time.LocalDate;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Date;

@SpringBootTest
public class CupomServiceTeste {

    @MockBean
    CupomRepository cupomRepository;

    @Autowired
    CupomService cupomService;

    private Cupom cupom;

    @BeforeEach
    public void setUp(){
        cupom = new Cupom();
    }

    @Test
    public void testarSalvarCupom()
    {
        LocalDate localDate = LocalDate.of(2020,11,20);
        Cupom cupom = new Cupom(1,"Páscoa",10, localDate);
        Mockito.when(cupomRepository.save(cupom)).thenReturn(cupom);

        Cupom cupomTeste = cupomService.cadastrarCupom(cupom);

        System.out.println(cupomTeste.getDataValidade());

        Assertions.assertEquals(cupom.getNomeCupom(), cupomTeste.getNomeCupom());
    }

    @Test
    public void testarSalvarCupomInvalido()
    {
        LocalDate localDate = LocalDate.of(2018,11,20);
        cupom = new Cupom(1,"Páscoa",10, localDate);
        Mockito.when(cupomRepository.save(cupom)).thenReturn(cupom);

        Assertions.assertThrows(RuntimeException.class, () -> {cupomService.cadastrarCupom(cupom);});
    }

    @Test
    public void testarDeletarCupom()
    {
        Mockito.when(cupomRepository.existsById(Mockito.anyInt())).thenReturn(true);

        cupomService.deleteCupom(8001);
        Mockito.verify(cupomRepository, Mockito.times(1)).deleteById(Mockito.anyInt());
    }

    @Test
    public void testarBuscarCupons(){
        Iterable<Cupom> cuponsTeste = Arrays.asList();
        Iterable<Cupom> cupons = cupomService.buscarCupons();
        Mockito.when(cupomRepository.findAll()).thenReturn(cuponsTeste);

        Assertions.assertEquals(cupons,cuponsTeste);
    }

    @Test
    public void testarBuscarCupomPorID(){
        Mockito.when(cupomRepository.findById(Mockito.anyInt())).thenReturn(java.util.Optional.ofNullable(cupom));
        Cupom cupomTeste = cupomService.buscarPorID(cupom.getId());
        Assertions.assertEquals(cupomTeste,cupom);
    }


}
