package br.com.controleestoque.services;

import br.com.controleestoque.enums.TipoPagamentoEnum;
import br.com.controleestoque.models.*;
import br.com.controleestoque.models.dtos.PedidoRespostaDTO;
import br.com.controleestoque.models.dtos.ProdutoEntradaDTO;
import br.com.controleestoque.repositories.PedidoRepository;
import br.com.controleestoque.repositories.ProdutoRepository;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.Mockito;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;

import java.time.LocalDate;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

@SpringBootTest

public class ProdutoTeste {

    @MockBean
    private ProdutoRepository produtoRepository;

    @Autowired
    private ProdutoService produtoService;

    @MockBean
    private EstoqueService estoqueService;

    Estoque estoque;
    Produto produto;
    List<Produto> produtos;
    ProdutoEntradaDTO produtoEntradaDTO;

    @BeforeEach
    public void setUp() {
        produto = new Produto();
        produto.setId(1);
        produto.setDescricaoProduto("Sandalia");
        produto.setValorUnitario(50.00);

        produtos = new ArrayList<>();
        produtos.add(produto);

        estoque = new Estoque();
        estoque.setId(1);
        estoque.setProduto(produto);
        estoque.setQuantidadeEstoque(10);

        produtoEntradaDTO = new ProdutoEntradaDTO("Sandalia", 50.00, 10);
    }

    @Test
    public void testarCadastrarProduto() {
        Mockito.when(produtoRepository.save(Mockito.any(Produto.class))).thenReturn(produto);

        Mockito.when(estoqueService.cadastrarEstoque(Mockito.any(Estoque.class))).thenReturn(estoque);

        Produto produtoObjeto = produtoService.cadastrarProduto(produtoEntradaDTO);

        Assertions.assertEquals(produto, produtoObjeto);
    }


//    @Test
//    public void testarAtualizarProduto() {
//        Mockito.when(produtoService.buscarPorId(Mockito.anyInt())).thenReturn(produto);
//
//        Mockito.when(estoqueService./buscarPorID(Mockito.any(Produto.class))).thenReturn(estoque);
//
//        Mockito.when(produtoRepository.save(Mockito.any(Produto.class))).thenReturn(produto);
//
//        long quantidade = 20;
//        Mockito.when(estoqueService.incrementarEstoque(Mockito.anyLong(), Mockito.anyLong())).thenReturn(quantidade);
//
//        Mockito.when(estoqueService.cadastrarEstoque(Mockito.any(Estoque.class))).thenReturn(estoque);
//
//        Produto produtoObjeto = produtoService.atualizarProduto(1, produtoEntradaDTO);
//
//        Assertions.assertEquals(produto, produtoObjeto);
//    }

//    @Test
//    public void testarAtualizarProdutoNegativo() {
//
//        Mockito.when(estoqueService.buscarPorID(Mockito.any(Produto.class))).thenReturn(estoque);
//
//        Produto produtoObjeto = produtoService.cadastrarProduto(produtoEntradaDTO);
//
//        Assertions.assertEquals(produto, produtoObjeto);
//    }
}
