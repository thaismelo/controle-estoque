package br.com.controleestoque.controllers;

import br.com.controleestoque.models.Cliente;
import br.com.controleestoque.models.dtos.PedidoRespostaDTO;
import br.com.controleestoque.security.JWTUtil;
import br.com.controleestoque.services.ClienteService;
import br.com.controleestoque.services.LoginUsuarioService;
import br.com.controleestoque.services.ProdutoService;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.hamcrest.CoreMatchers;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.Mockito;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.MediaType;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.ResultActions;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;
import org.springframework.test.web.servlet.result.MockMvcResultMatchers;

import java.time.LocalDate;
import java.util.Arrays;
import java.util.List;

@WebMvcTest(ClienteController.class)
public class ClienteControllerTeste {

    @MockBean
    private JWTUtil jwtUtil;

    @MockBean
    private LoginUsuarioService loginUsuarioService;

    @Autowired
    private MockMvc mockMvc;

    @MockBean
    private ClienteService clienteService;

    Cliente cliente;

    @BeforeEach
    public void setUp(){
        cliente = new Cliente();
        cliente.setCpf("55043272023");
        cliente.setEmail("cliente@gmail.com");
        cliente.setNome("Maria");
        cliente.setId(1);
    }

    @Test
    public void testarCadastrarCliente() throws Exception {

        Mockito.when(clienteService.cadastrarCliente(Mockito.any(Cliente.class))).then(clienteObjeto -> {
            cliente.setId(1);
            return cliente;
        });

        ObjectMapper mapper = new ObjectMapper();
        String jsonDeCliente= mapper.writeValueAsString(cliente);

        ResultActions resultActions = mockMvc.perform(MockMvcRequestBuilders.post("/cliente")
                .contentType(MediaType.APPLICATION_JSON)
                .content(jsonDeCliente))
                .andExpect(MockMvcResultMatchers.status().isCreated());
    }

    @Test
    public void testarBuscarPorEmailNaoEncontrado() throws Exception {

        Mockito.when(clienteService.buscarClientePorEmail(Mockito.anyString())).thenThrow(RuntimeException.class);

        mockMvc.perform(MockMvcRequestBuilders.get("/cliente/clientes@gmail.com")
                .contentType(MediaType.APPLICATION_JSON))
                .andExpect(MockMvcResultMatchers.status().isBadRequest());
    }

    @Test
    public void testarBuscarPorEmail() throws Exception {
        cliente.setId(1);
        Mockito.when(clienteService.buscarClientePorEmail(Mockito.anyString())).thenReturn(cliente);

        mockMvc.perform(MockMvcRequestBuilders.get("/cliente/cliente@gmail.com")
                .contentType(MediaType.APPLICATION_JSON))
                .andExpect(MockMvcResultMatchers.jsonPath("$.id", CoreMatchers.equalTo(1)));
    }
}
