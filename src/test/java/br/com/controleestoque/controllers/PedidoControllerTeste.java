package br.com.controleestoque.controllers;

import br.com.controleestoque.enums.TipoPagamentoEnum;
import br.com.controleestoque.models.*;
import br.com.controleestoque.models.dtos.PedidoClienteRespostaDTO;
import br.com.controleestoque.models.dtos.PedidoRespostaDTO;
import br.com.controleestoque.security.JWTUtil;
import br.com.controleestoque.services.*;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.Mockito;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.MediaType;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.ResultActions;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;
import org.springframework.test.web.servlet.result.MockMvcResultMatchers;

import java.time.LocalDate;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

@WebMvcTest(PedidoController.class)
public class PedidoControllerTeste {

    @Autowired
    private MockMvc mockMvc;

    @MockBean
    private PedidoService pedidoService;

    @MockBean
    private ClienteService clienteService;

    @MockBean
    private ProdutoService produtoService;

    @MockBean
    private EstoqueService estoqueService;

    @MockBean
    private CupomService cupomService;

    @MockBean
    private JWTUtil jwtUtil;

    @MockBean
    private LoginUsuarioService loginUsuarioService;

    Pedido pedido;
    Cliente cliente;
    Cupom cupom;
    Estoque estoque;
    Produto produto;
    List<Produto> produtos;
    PedidoRespostaDTO pedidoRespostaDTO;
    PedidoClienteRespostaDTO pedidoClienteRespostaDTO;

    @BeforeEach
    public void setUp(){
        cliente = new Cliente();
        cliente.setCpf("55043272023");
        cliente.setEmail("cliente@gmail.com");
        cliente.setNome("Maria");
        cliente.setId(1);

        cupom = new Cupom();
        //cupom.setDataValidade(LocalDate.now());
        cupom.setNomeCupom("PROMO10");
        cupom.setPorcentagemDesconto(10);
        cupom.setId(1);

        produto = new Produto();
        produto.setId(1);
        produto.setDescricaoProduto("Sandalia");
        produto.setValorUnitario(50.00);

        produtos = new ArrayList<>();
        produtos.add(produto);

        estoque = new Estoque(10 , produto);

        pedido = new Pedido();
        pedido.setCliente(cliente);
       // pedido.setDataDaCompra(LocalDate.now());
        pedido.setProdutos(produtos);
        pedido.setCupom(cupom);
        //pedido.setId(1);
        pedido.setTipoPagamento(TipoPagamentoEnum.CARTAOCREDITO);
        pedido.setValorTotal(45.00);

        pedidoRespostaDTO = new PedidoRespostaDTO(pedido.getValorTotal(),pedido.getProdutos().size(), pedido.getTipoPagamento(),
                pedido.getDataDaCompra());

        pedidoClienteRespostaDTO = new PedidoClienteRespostaDTO(cliente.getNome(),pedido.getDataDaCompra(),
                produto.getDescricaoProduto(), pedido.getValorTotal());

    }

    @Test
    public void testarListarValorDoDiaPorData() throws Exception {
        Iterable<PedidoRespostaDTO> pedidoRespostaDTOS = Arrays.asList(pedidoRespostaDTO);

        Mockito.when(pedidoService.listarValorTotalDoDia(Mockito.any(LocalDate.class))).thenReturn((List<PedidoRespostaDTO>) pedidoRespostaDTOS);

        mockMvc.perform(MockMvcRequestBuilders.get("/pedidos?dataCompra=2020-06-29")
                .contentType(MediaType.APPLICATION_JSON))
                .andExpect(MockMvcResultMatchers.jsonPath("$.length()").value(1));
    }

    @Test
    public void testarListarPedidosPorTipoDePagamento() throws Exception {
        Iterable<PedidoRespostaDTO> pedidoRespostaDTOS = Arrays.asList(pedidoRespostaDTO);

        Mockito.when(pedidoService.listarPorTipoPagamento(Mockito.any(TipoPagamentoEnum.class))).thenReturn((List<PedidoRespostaDTO>) pedidoRespostaDTOS);

        mockMvc.perform(MockMvcRequestBuilders.get("/pedidos/totalPorTipoPagamento/CARTAOCREDITO")
                .contentType(MediaType.APPLICATION_JSON))
                .andExpect(MockMvcResultMatchers.jsonPath("$.length()").value(1));
    }

    @Test
    public void testarListarValorDoDia() throws Exception {
        Iterable<PedidoRespostaDTO> pedidoRespostaDTOS = Arrays.asList(pedidoRespostaDTO);

        Mockito.when(pedidoService.listarValorTotal()).thenReturn((List<PedidoRespostaDTO>) pedidoRespostaDTOS);

        mockMvc.perform(MockMvcRequestBuilders.get("/pedidos")
                .contentType(MediaType.APPLICATION_JSON))
                .andExpect(MockMvcResultMatchers.jsonPath("$.length()").value(1));
    }

    @Test
    public void testarRegistrarPedidoSemSucesso() throws Exception {

        Mockito.when(pedidoService.registrarCompra(Mockito.any(Pedido.class))).thenThrow(RuntimeException.class);

        ObjectMapper mapper = new ObjectMapper();
        String jsonDeLead = mapper.writeValueAsString(pedido);

        mockMvc.perform(MockMvcRequestBuilders.post("/pedidos")
                .contentType(MediaType.APPLICATION_JSON)
                .content(jsonDeLead))
                .andExpect(MockMvcResultMatchers.status().isBadRequest());
    }

    @Test
    public void testarRegistrarPedidoComSucesso() throws Exception {

       Mockito.when(pedidoService.registrarCompra(Mockito.any(Pedido.class))).then(pedidoObjeto -> {
            pedido.setId(1);
            return pedidoRespostaDTO;
        });

       ObjectMapper mapper = new ObjectMapper();
        String jsonDePedido= mapper.writeValueAsString(pedido);

        ResultActions resultActions = mockMvc.perform(MockMvcRequestBuilders.post("/pedidos")
                .contentType(MediaType.APPLICATION_JSON)
                .content(jsonDePedido))
                .andExpect(MockMvcResultMatchers.status().isCreated());
    }

    @Test
    public void testarListarPedidosPorCliente() throws Exception {
        Iterable<PedidoClienteRespostaDTO> pedidoClienteRespostaDTOS = Arrays.asList(pedidoClienteRespostaDTO);

        Mockito.when(pedidoService.listarPedidoPorCliente(Mockito.anyInt())).thenReturn((List<PedidoClienteRespostaDTO>) pedidoClienteRespostaDTOS);

        mockMvc.perform(MockMvcRequestBuilders.get("/pedidos/clientes/1")
                .contentType(MediaType.APPLICATION_JSON))
                .andExpect(MockMvcResultMatchers.jsonPath("$.length()").value(1));
    }

}
