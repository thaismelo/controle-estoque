package br.com.controleestoque.controllers;

import br.com.controleestoque.enums.TipoPagamentoEnum;
import br.com.controleestoque.models.*;
import br.com.controleestoque.models.dtos.PedidoClienteRespostaDTO;
import br.com.controleestoque.models.dtos.PedidoRespostaDTO;
import br.com.controleestoque.models.dtos.ProdutoEntradaDTO;
import br.com.controleestoque.models.dtos.ProdutoRespostaDTO;
import br.com.controleestoque.security.JWTUtil;
import br.com.controleestoque.services.EstoqueService;
import br.com.controleestoque.services.LoginUsuarioService;
import br.com.controleestoque.services.PedidoService;
import br.com.controleestoque.services.ProdutoService;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.hamcrest.CoreMatchers;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.Mockito;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.MediaType;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.ResultActions;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;
import org.springframework.test.web.servlet.result.MockMvcResultMatchers;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

@WebMvcTest(ProdutoController.class)
public class ProdutoControllerTeste {

    @MockBean
    private JWTUtil jwtUtil;

    @MockBean
    private LoginUsuarioService loginUsuarioService;

    @Autowired
    private MockMvc mockMvc;

    @MockBean
    private ProdutoService produtoService;

    @MockBean
    private EstoqueService estoqueService;

    Estoque estoque;
    Produto produto;
    ProdutoEntradaDTO produtoEntradaDTO;
    ProdutoRespostaDTO produtoRespostaDTO;
    List<Produto> produtos;

    @BeforeEach
    public void setUp(){
        produto = new Produto();
        produto.setId(1);
        produto.setDescricaoProduto("Sandalia");
        produto.setValorUnitario(50.00);

        produtos = new ArrayList<>();
        produtos.add(produto);

        estoque = new Estoque(10 , produto);

        produtoEntradaDTO = new ProdutoEntradaDTO(produto.getDescricaoProduto(),produto.getValorUnitario(),estoque.getQuantidadeEstoque());

        produtoRespostaDTO = new ProdutoRespostaDTO(produto.getDescricaoProduto(),produto.getValorUnitario(),estoque.getQuantidadeEstoque());
    }

    @Test
    public void testarCadastrarProduto() throws Exception {

        Mockito.when(produtoService.cadastrarProduto(produtoEntradaDTO)).then(produtoObjeto -> {
            produto.setId(1);
            return produto;
        });

        ObjectMapper mapper = new ObjectMapper();
        String jsonDePedido= mapper.writeValueAsString(produto);

        ResultActions resultActions = mockMvc.perform(MockMvcRequestBuilders.post("/produto")
                .contentType(MediaType.APPLICATION_JSON)
                .content(jsonDePedido))
                .andExpect(MockMvcResultMatchers.status().isCreated());
    }

    @Test
    public void testarBuscarPorID() throws Exception {
        produto.setId(1);
        Mockito.when(produtoService.buscarPorId(Mockito.anyInt())).thenReturn(produto);

        mockMvc.perform(MockMvcRequestBuilders.get("/produto/1")
                .contentType(MediaType.APPLICATION_JSON))
                .andExpect(MockMvcResultMatchers.jsonPath("$.id", CoreMatchers.equalTo(1)));
    }

    @Test
    public void testarBuscarPorIdNaoEncontrado() throws Exception {
        Produto produtoObjeto = new Produto();
        Mockito.when(produtoService.buscarPorId(Mockito.anyInt())).thenThrow(RuntimeException.class);

        mockMvc.perform(MockMvcRequestBuilders.get("/produto/20")
                .contentType(MediaType.APPLICATION_JSON))
                .andExpect(MockMvcResultMatchers.status().isBadRequest());
    }

    @Test
    public void testarExibirTodosOsProdutos() throws Exception {
        Iterable<Produto> produtos = Arrays.asList(produto);
        Mockito.when(produtoService.listarProdutos()).thenReturn(produtos);

        mockMvc.perform(MockMvcRequestBuilders.get("/produto")
                .contentType(MediaType.APPLICATION_JSON))
                .andExpect(MockMvcResultMatchers.jsonPath("$.length()").value(1));
    }

    @Test
    public void testarAtualizarProdutoNaoEncontrado() throws Exception {
        produto.setId(1);
        Mockito.when(produtoService.atualizarProduto(Mockito.anyInt(), Mockito.any(ProdutoEntradaDTO.class))).
                thenThrow(RuntimeException.class);

        ObjectMapper mapper = new ObjectMapper();
        String jsonDeLead = mapper.writeValueAsString(produto);

        mockMvc.perform(MockMvcRequestBuilders.put("/produto/5")
                .contentType(MediaType.APPLICATION_JSON)
                .content(jsonDeLead))
                .andExpect(MockMvcResultMatchers.status().isBadRequest());
    }

    @Test
    public void testarAtualizarProdutoEncontrado() throws Exception {
        produto.setId(1);
        Mockito.when(produtoService.atualizarProduto(Mockito.anyInt(), Mockito.any(ProdutoEntradaDTO.class)))
                .thenReturn(produto);

        ObjectMapper mapper = new ObjectMapper();
        String jsonDeLead = mapper.writeValueAsString(produto);

        mockMvc.perform(MockMvcRequestBuilders.put("/produto/1")
                .contentType(MediaType.APPLICATION_JSON)
                .content(jsonDeLead))
                .andExpect(MockMvcResultMatchers.jsonPath("$.id").value(1));
    }

    @Test
    public void testarListarProdutosSemEstoqueDisponivel() throws Exception {

        estoque.setQuantidadeEstoque(0);
        Iterable<ProdutoRespostaDTO> produtoRespostaDTOS = Arrays.asList(produtoRespostaDTO);

        Mockito.when(produtoService.listarProdutosComOuSemEstoque(Mockito.anyBoolean())).thenReturn((List<ProdutoRespostaDTO>) produtoRespostaDTOS);

        mockMvc.perform(MockMvcRequestBuilders.get("/produto/estoquedisponivel/0")
                .contentType(MediaType.APPLICATION_JSON))
                .andExpect(MockMvcResultMatchers.jsonPath("$.length()").value(1));
    }

    @Test
    public void testarListarProdutosComEstoqueDisponivel() throws Exception {

        Iterable<ProdutoRespostaDTO> produtoRespostaDTOS = Arrays.asList(produtoRespostaDTO);

        Mockito.when(produtoService.listarProdutosComOuSemEstoque(Mockito.anyBoolean())).thenReturn((List<ProdutoRespostaDTO>) produtoRespostaDTOS);

        mockMvc.perform(MockMvcRequestBuilders.get("/produto/estoquedisponivel/1")
                .contentType(MediaType.APPLICATION_JSON))
                .andExpect(MockMvcResultMatchers.jsonPath("$.length()").value(1));
    }
}
