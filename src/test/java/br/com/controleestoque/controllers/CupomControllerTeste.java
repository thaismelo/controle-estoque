package br.com.controleestoque.controllers;

import br.com.controleestoque.models.Cupom;
import br.com.controleestoque.models.Pedido;
import br.com.controleestoque.models.dtos.PedidoRespostaDTO;
import br.com.controleestoque.security.JWTUtil;
import br.com.controleestoque.services.CupomService;
import br.com.controleestoque.services.LoginUsuarioService;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.hamcrest.CoreMatchers;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.Mockito;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.MediaType;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.ResultActions;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;
import org.springframework.test.web.servlet.result.MockMvcResultMatchers;

import java.time.LocalDate;
import java.util.Arrays;
import java.util.List;

@WebMvcTest(CupomController.class)
public class CupomControllerTeste {

    @MockBean
    CupomService cupomService;

    @Autowired
    MockMvc mockMvc;

    @MockBean
    private JWTUtil jwtUtil;

    @MockBean
    private LoginUsuarioService loginUsuarioService;

    private Cupom cupom;

    @BeforeEach
    private void setUp() {
        LocalDate localDate = LocalDate.of(2020, 11, 20);
        cupom = new Cupom();
        //cupom.setDataValidade(localDate);
        cupom.setNomeCupom("Páscoa");
        cupom.setPorcentagemDesconto(10);
    }

    @Test
    public void testarCadastrarCupom() throws Exception {
        Mockito.when(cupomService.cadastrarCupom(Mockito.any(Cupom.class))).then(cupomObjeto -> {
            cupom.setId(1);
            return cupom;
        });

        ObjectMapper mapper = new ObjectMapper();
        String jsonDeCupom= mapper.writeValueAsString(cupom);

        ResultActions resultActions = mockMvc.perform(MockMvcRequestBuilders.post("/cupom")
                .contentType(MediaType.APPLICATION_JSON)
                .content(jsonDeCupom))
                .andExpect(MockMvcResultMatchers.status().isCreated());
    }

    @Test
    public void testarBuscarTodos() throws Exception{
        Iterable<Cupom> cupons = Arrays.asList(cupom);

        Mockito.when(cupomService.buscarCupons()).thenReturn(cupons);

        mockMvc.perform(MockMvcRequestBuilders.get("/cupom")
                .contentType(MediaType.APPLICATION_JSON))
                .andExpect(MockMvcResultMatchers.jsonPath("$.length()").value(1));
    }

    @Test
    public void testarDeletarCupom() throws Exception{
        ResultActions resultActions = mockMvc.perform(MockMvcRequestBuilders.delete("/cupom/{id}",1)
                .contentType(MediaType.APPLICATION_JSON)
                .accept(MediaType.APPLICATION_JSON))
                .andExpect(MockMvcResultMatchers.status().isNoContent());
    }

    @Test
    public void testarBuscarPorID() throws Exception{
        Mockito.when(cupomService.buscarPorID(Mockito.anyInt())).then(cupomObjeto -> {
            cupom.setId(1);
            return cupom;
        });

        mockMvc.perform(MockMvcRequestBuilders.get("/cupom/{id}",1)
                .contentType(MediaType.APPLICATION_JSON))
                .andExpect(MockMvcResultMatchers.jsonPath("$.id").value(1));
    }

}


