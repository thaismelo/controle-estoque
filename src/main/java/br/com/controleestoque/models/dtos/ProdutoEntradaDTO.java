package br.com.controleestoque.models.dtos;

import br.com.controleestoque.models.Produto;

import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.validation.constraints.Digits;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

public class ProdutoEntradaDTO {

    @Size(min = 1, max = 100, message = "A descricao deve ter entre 1 à 100 caracteres")
    @NotNull(message = "Descrição não pode ser nullo")
    private String descricaoProduto;


    @Digits(integer = 10, fraction = 2, message = "O valor está no formato incorreto")
    private double valorUnitario;

    private long quantidade;

    public ProdutoEntradaDTO(@Size(min = 1, max = 100, message = "A descricao deve ter entre 1 à 100 caracteres") @NotNull(message = "Descrição não pode ser nullo") String descricaoProduto, @Digits(integer = 10, fraction = 2, message = "O valor está no formato incorreto") double valorUnitario, long quantidade) {
        this.descricaoProduto = descricaoProduto;
        this.valorUnitario = valorUnitario;
        this.quantidade = quantidade;
    }

    public String getDescricaoProduto() {
        return descricaoProduto;
    }

    public void setDescricaoProduto(String descricaoProduto) {
        this.descricaoProduto = descricaoProduto;
    }

    public double getValorUnitario() {
        return valorUnitario;
    }

    public void setValorUnitario(double valorUnitario) {
        this.valorUnitario = valorUnitario;
    }

    public long getQuantidade() {
        return quantidade;
    }

    public void setQuantidade(long quantidade) {
        this.quantidade = quantidade;
    }
}