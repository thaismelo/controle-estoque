package br.com.controleestoque.models.dtos;

import java.time.LocalDate;

public class PedidoClienteRespostaDTO {
    private String nomeCliente;
    private LocalDate dataDaCompra;
    private String produto;
    private double ValorTotal;

    public PedidoClienteRespostaDTO(String nomeCliente, LocalDate dataDaCompra, String produto, double valorTotal) {
        this.nomeCliente = nomeCliente;
        this.dataDaCompra = dataDaCompra;
        this.produto = produto;
        ValorTotal = valorTotal;
    }

    public String getNomeCliente() {
        return nomeCliente;
    }

    public void setNomeCliente(String nomeCliente) {
        this.nomeCliente = nomeCliente;
    }

    public LocalDate getDataDaCompra() {
        return dataDaCompra;
    }

    public void setDataDaCompra(LocalDate dataDaCompra) {
        this.dataDaCompra = dataDaCompra;
    }

    public String getProduto() {
        return produto;
    }

    public void setProduto(String produto) {
        this.produto = produto;
    }

    public double getValorTotal() {
        return ValorTotal;
    }

    public void setValorTotal(double valorTotal) {
        ValorTotal = valorTotal;
    }
}
