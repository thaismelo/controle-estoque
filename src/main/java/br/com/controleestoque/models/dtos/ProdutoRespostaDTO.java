package br.com.controleestoque.models.dtos;

import javax.validation.constraints.Digits;

public class ProdutoRespostaDTO {

    private String descricaoProduto;

    @Digits(integer = 10, fraction = 2, message = "O valor está no formato incorreto")
    private double valorUnitario;

    private long quantidade;

    public ProdutoRespostaDTO(String descricaoProduto, @Digits(integer = 10, fraction = 2, message = "O valor está no formato incorreto") double valorUnitario, long quantidade) {
        this.descricaoProduto = descricaoProduto;
        this.valorUnitario = valorUnitario;
        this.quantidade = quantidade;
    }

    public String getDescricaoProduto() {
        return descricaoProduto;
    }

    public void setDescricaoProduto(String descricaoProduto) {
        this.descricaoProduto = descricaoProduto;
    }

    public double getValorUnitario() {
        return valorUnitario;
    }

    public void setValorUnitario(double valorUnitario) {
        this.valorUnitario = valorUnitario;
    }

    public long getQuantidade() {
        return quantidade;
    }

    public void setQuantidade(long quantidade) {
        this.quantidade = quantidade;
    }
}
