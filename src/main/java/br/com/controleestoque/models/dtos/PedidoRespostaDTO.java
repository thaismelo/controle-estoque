package br.com.controleestoque.models.dtos;

import br.com.controleestoque.enums.TipoPagamentoEnum;

import java.math.BigDecimal;
import java.math.RoundingMode;
import java.time.LocalDate;

public class PedidoRespostaDTO {
    private double valorTotalDaCompra;
    private long quantidadeTotalItens;
    private TipoPagamentoEnum tipoPagamentoEnum;
    private LocalDate dataDaCompra;

    public PedidoRespostaDTO(double valorTotalDaCompra, long quantidadeTotalItens, TipoPagamentoEnum tipoPagamentoEnum,
                             LocalDate dataDaCompra) {
        this.valorTotalDaCompra = valorTotalDaCompra;
        this.quantidadeTotalItens = quantidadeTotalItens;
        this.tipoPagamentoEnum = tipoPagamentoEnum;
        this.dataDaCompra = dataDaCompra;
    }

    public double getValorTotalDaCompra() {
        BigDecimal bd = new BigDecimal(valorTotalDaCompra).setScale(2, RoundingMode.FLOOR);
        return bd.doubleValue();
        //return valorTotalDaCompra;
    }

    public void setValorTotalDaCompra(double valorTotalDaCompra) {
        BigDecimal bd = new BigDecimal(valorTotalDaCompra).setScale(2, RoundingMode.FLOOR);
        this.valorTotalDaCompra = bd.doubleValue();
    }

    public long getQuantidadeTotalItens() {
        return quantidadeTotalItens;
    }

    public void setQuantidadeTotalItens(long quantidadeTotalItens) {
        this.quantidadeTotalItens = quantidadeTotalItens;
    }

    public TipoPagamentoEnum getTipoPagamentoEnum() {
        return tipoPagamentoEnum;
    }

    public void setTipoPagamentoEnum(TipoPagamentoEnum tipoPagamentoEnum) {
        this.tipoPagamentoEnum = tipoPagamentoEnum;
    }

    public LocalDate getDataDaCompra() {
        return dataDaCompra;
    }

    public void setDataDaCompra(LocalDate dataDaCompra) {
        this.dataDaCompra = dataDaCompra;
    }
}
