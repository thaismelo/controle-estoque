package br.com.controleestoque.models;

import br.com.controleestoque.enums.TipoPagamentoEnum;
import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import org.springframework.format.annotation.DateTimeFormat;

import javax.persistence.*;
import javax.validation.constraints.Digits;
import javax.validation.constraints.NotNull;
import java.math.BigDecimal;
import java.math.RoundingMode;
import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.util.List;

@Entity
public class Pedido {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @JsonIgnoreProperties(value = {"dataDaCompra", "valorTotal"}, allowGetters = true)
    private int id;

    @DateTimeFormat(iso = DateTimeFormat.ISO.DATE)
    @JsonFormat(pattern = "YYYY-MM-dd")
    private LocalDate dataDaCompra;

    @Digits(integer = 10, fraction = 2 ,message = "Valor está no formato incorreto")
    private double valorTotal;

    @Enumerated(EnumType.STRING)
    @NotNull(message = "É obrigatório selecionar a forma de pagamento")
    private TipoPagamentoEnum tipoPagamento;

    @ManyToMany(cascade = CascadeType.ALL)
    @NotNull(message = "É obrigatório ter produtos no carrinho de compras")
    private List<Produto> produtos;

    @ManyToOne(cascade = CascadeType.ALL)
    @NotNull(message = "É obrigatório o preenchimento do cliente")
    private Cliente cliente;

    @ManyToOne(cascade = CascadeType.ALL)
    private Cupom cupom;

    public Pedido() {
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public LocalDate getDataDaCompra() {
        return dataDaCompra;
    }

    public void setDataDaCompra(LocalDate dataDaCompra) {
        this.dataDaCompra = dataDaCompra;
    }

    public double getValorTotal() {
        BigDecimal bd = new BigDecimal(valorTotal).setScale(2, RoundingMode.FLOOR);
        return bd.doubleValue();
       // return valorTotal;
    }

    public void setValorTotal(double valorTotal) {
        BigDecimal bd = new BigDecimal(valorTotal).setScale(2, RoundingMode.FLOOR);
        this.valorTotal = bd.doubleValue();
    }

    public TipoPagamentoEnum getTipoPagamento() {
        return tipoPagamento;
    }

    public void setTipoPagamento(TipoPagamentoEnum tipoPagamento) {
        this.tipoPagamento = tipoPagamento;
    }

    public List<Produto> getProdutos() {
        return produtos;
    }

    public void setProdutos(List<Produto> produtos) {
        this.produtos = produtos;
    }

    public Cliente getCliente() {
        return cliente;
    }

    public void setCliente(Cliente cliente) {
        this.cliente = cliente;
    }

    public Cupom getCupom() {
        return cupom;
    }

    public void setCupom(Cupom cupom) {
        this.cupom = cupom;
    }
}
