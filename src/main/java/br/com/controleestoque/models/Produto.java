package br.com.controleestoque.models;

import org.springframework.validation.annotation.Validated;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.validation.constraints.Digits;
import javax.validation.constraints.Min;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

@Entity
public class Produto {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private int id;

    @Size(min = 1, max = 100, message = "A descricao deve ter entre 1 à 100 caracteres")
    @NotNull(message = "Descrição não pode ser nullo")
    private String descricaoProduto;


    @Digits(integer = 10, fraction = 2, message = "O valor está no formato incorreto")
    private double valorUnitario;

    public Produto() {
    }

    public Produto(int id, String descricaoProduto, double valorUnitario) {
        this.id = id;
        this.descricaoProduto = descricaoProduto;
        this.valorUnitario = valorUnitario;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getDescricaoProduto() {
        return descricaoProduto;
    }

    public void setDescricaoProduto(String descricaoProduto) {
        this.descricaoProduto = descricaoProduto;
    }

    public double getValorUnitario() {
        return valorUnitario;
    }

    public void setValorUnitario(double valorUnitario) {
        this.valorUnitario = valorUnitario;
    }
}
