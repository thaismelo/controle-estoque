package br.com.controleestoque.models;

import javax.persistence.*;
import javax.validation.constraints.Min;
import javax.validation.constraints.NotNull;

@Entity
public class Estoque {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private int id;

    @Min(0)
    @NotNull(message = "Quantidade não pode ser nullo")
    private long quantidadeEstoque;

    @OneToOne(cascade = CascadeType.ALL)
    private Produto produto;

    public Estoque() {
    }

    public Estoque(@Min(0) @NotNull(message = "Quantidade não pode ser nullo") long quantidadeEstoque, Produto produto) {
        this.quantidadeEstoque = quantidadeEstoque;
        this.produto = produto;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public long getQuantidadeEstoque() {
        return quantidadeEstoque;
    }

    public void setQuantidadeEstoque(long quantidadeEstoque) {
        this.quantidadeEstoque = quantidadeEstoque;
    }

    public Produto getProduto() {
        return produto;
    }

    public void setProduto(Produto produto) {
        this.produto = produto;
    }

    public void reduzirEstoque(int quantidade) {
        this.quantidadeEstoque = quantidadeEstoque - quantidade;
    }

    public void incrementarEstoque(int quantidade) {
        this.quantidadeEstoque = quantidadeEstoque + quantidade;
    }
}
