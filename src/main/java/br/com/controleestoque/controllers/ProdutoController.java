package br.com.controleestoque.controllers;

import br.com.controleestoque.models.Produto;
import br.com.controleestoque.models.dtos.ProdutoEntradaDTO;
import br.com.controleestoque.models.dtos.ProdutoRespostaDTO;
import br.com.controleestoque.services.ProdutoService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.server.ResponseStatusException;

import javax.validation.Valid;
import java.util.List;

@RestController
@RequestMapping("/produto")
public class ProdutoController {

    @Autowired
    private ProdutoService produtoService;

    @PostMapping
    @ResponseStatus(HttpStatus.CREATED)
    public Produto registarProduto(@RequestBody @Valid ProdutoEntradaDTO produtoEntradaDTO){
        //ProdutoEntradaDTO produtoEntradaDTO = new ProdutoEntradaDTO(produtoEntradaDTO.getProduto(), produtoEntradaDTO.getQuantidade());
        return produtoService.cadastrarProduto(produtoEntradaDTO);
    }

    @GetMapping
    public Iterable<Produto> exibirTodos(){

        Iterable<Produto> produtos = produtoService.listarProdutos();
        return produtos;
    }

    @GetMapping("/{id}")
    public Produto bucarPorId(@PathVariable(name = "id") int id){
        try{
            Produto produto = produtoService.buscarPorId(id);
            return produto;
        }catch (RuntimeException exception){
            throw new ResponseStatusException(HttpStatus.BAD_REQUEST, exception.getMessage());
        }
    }

   @PutMapping("/{id}")
    public Produto atualizarProduto(@PathVariable(name = "id") int id, @RequestBody ProdutoEntradaDTO produtoEntradaDTO){
       try{

           Produto produtoObjeto = produtoService.atualizarProduto(id, produtoEntradaDTO);
            return produtoObjeto;

       }catch (RuntimeException exception){
           throw new ResponseStatusException(HttpStatus.BAD_REQUEST, exception.getMessage());
       }
    }

    @GetMapping("/estoquedisponivel/{estoque}")
    public List<ProdutoRespostaDTO> listarProdutosEmEstoque(@PathVariable(name = "estoque") boolean existeEstoque){
        try{
            List<ProdutoRespostaDTO> produtosEmEstoque = produtoService.listarProdutosComOuSemEstoque(existeEstoque);
            return produtosEmEstoque;

        }catch (RuntimeException exception){
            throw new ResponseStatusException(HttpStatus.BAD_REQUEST, exception.getMessage());
        }
    }

//
//    @DeleteMapping("/{id}")
//    public ResponseEntity<?> deletarProduto(@PathVariable Long id){
//        try{
//            produtoService.deletarProduto(id);
//            return ResponseEntity.status(204).body("");
//        }catch (RuntimeException exception){
//            throw new ResponseStatusException(HttpStatus.BAD_REQUEST, exception.getMessage());
//        }
//    }
}
