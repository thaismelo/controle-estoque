package br.com.controleestoque.controllers;

import br.com.controleestoque.enums.TipoPagamentoEnum;
import br.com.controleestoque.models.Pedido;
import br.com.controleestoque.models.dtos.PedidoClienteRespostaDTO;
import br.com.controleestoque.models.dtos.PedidoRespostaDTO;
import br.com.controleestoque.services.PedidoService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.format.annotation.DateTimeFormat;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.server.ResponseStatusException;

import javax.validation.Valid;
import java.time.LocalDate;
import java.util.List;

@RestController
@RequestMapping("/pedidos")
public class PedidoController {

    @Autowired
    private PedidoService pedidoService;

    @PostMapping
    @ResponseStatus(HttpStatus.CREATED)
    public PedidoRespostaDTO registrarProduto(@RequestBody @Valid Pedido pedido){
        try {
            return pedidoService.registrarCompra(pedido);
        }catch (RuntimeException excecao){
            throw new ResponseStatusException(HttpStatus.BAD_REQUEST, excecao.getMessage());
        }
    }

    @GetMapping
    public List<PedidoRespostaDTO> listarValorTotal(@RequestParam(name = "dataCompra", required = false)
                                                        @DateTimeFormat(iso = DateTimeFormat.ISO.DATE) LocalDate dataCompra){
        if(dataCompra != null){
            List<PedidoRespostaDTO> pedidos = pedidoService.listarValorTotalDoDia(dataCompra);
            return pedidos;
        }

        List<PedidoRespostaDTO> pedidos = pedidoService.listarValorTotal();
        return pedidos;
    }

    @GetMapping("/totalPorTipoPagamento/{tipoPagamento}")
    public List<PedidoRespostaDTO> listarTipoPagamento(@PathVariable(name = "tipoPagamento", required = true)
                                                               TipoPagamentoEnum tipoPagamentoEnum) {

        List<PedidoRespostaDTO> pedidos = pedidoService.listarPorTipoPagamento(tipoPagamentoEnum);

        return pedidos;
    }

    @GetMapping("/clientes/{id}")
    public List<PedidoClienteRespostaDTO> listarPedidoPorCliente (@PathVariable(name = "id", required = true)
                                                                  int id){

        List<PedidoClienteRespostaDTO> pedidos = pedidoService.listarPedidoPorCliente(id);

        return pedidos;
    }
}
