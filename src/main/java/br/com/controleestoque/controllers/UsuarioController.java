package br.com.controleestoque.controllers;

import br.com.controleestoque.models.Usuario;
import br.com.controleestoque.services.UsuarioService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;

@RestController
@RequestMapping("/usuario")
public class UsuarioController {

    @Autowired
    private UsuarioService usuarioService;

    @PostMapping
    @ResponseStatus(HttpStatus.CREATED)
    public Usuario registrarUsuario(@RequestBody @Valid Usuario usuario){
        Usuario usuarioObjeto = usuarioService.salvarUsuario(usuario);
        return usuarioObjeto;
    }


}
