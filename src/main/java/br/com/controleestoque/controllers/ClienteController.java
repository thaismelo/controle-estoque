package br.com.controleestoque.controllers;

import br.com.controleestoque.models.Cliente;
import br.com.controleestoque.services.ClienteService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.server.ResponseStatusException;

import javax.validation.Valid;

@RestController
@RequestMapping("/cliente")
public class ClienteController {

    @Autowired
    private ClienteService clienteService;

    @PostMapping
    @ResponseStatus(HttpStatus.CREATED)
    public Cliente cadastrarCliente(@RequestBody @Valid Cliente cliente){
        Cliente clienteObjeto = clienteService.cadastrarCliente(cliente);
        return clienteObjeto;
    }

    @GetMapping("/{email}")
    public Cliente buscarPorEmail(@PathVariable(name = "email") String email){

        try{
            Cliente cliente = clienteService.buscarClientePorEmail(email);
            return cliente;

        } catch (RuntimeException exception){
            throw new ResponseStatusException(HttpStatus.BAD_REQUEST, exception.getMessage());
        }

    }


}
