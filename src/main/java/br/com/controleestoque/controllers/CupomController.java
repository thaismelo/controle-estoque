package br.com.controleestoque.controllers;

import br.com.controleestoque.models.Cupom;
import br.com.controleestoque.services.CupomService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.server.ResponseStatusException;

import javax.persistence.GeneratedValue;
import javax.validation.Valid;

@RestController
@RequestMapping("/cupom")
public class CupomController {

    @Autowired
    CupomService cupomService;

    @PostMapping()
    @ResponseStatus(HttpStatus.CREATED)
    public Cupom salvarCupom(@RequestBody @Valid Cupom cupom)
    {
        try {
            Cupom cupomObjeto = cupomService.cadastrarCupom(cupom);
            return cupomObjeto;
        }catch (RuntimeException excecao){
            throw new ResponseStatusException(HttpStatus.BAD_REQUEST, excecao.getMessage());
        }

    }

    @GetMapping()
    public Iterable<Cupom> buscarTodos()
    {
        Iterable<Cupom> cupoms = cupomService.buscarCupons();
        return cupoms;
    }

    @DeleteMapping("/{id}")
    @ResponseStatus(HttpStatus.NO_CONTENT)
    public void deletarCupom(@PathVariable int id){
        try{
            cupomService.deleteCupom(id);
        }catch (RuntimeException exception){
            throw new ResponseStatusException(HttpStatus.BAD_REQUEST, exception.getMessage());
        }
    }

    @GetMapping("/{id}")
    public Cupom buscarCupomPorID(@PathVariable int id){
        try{
            Cupom cupom = cupomService.buscarPorID(id);
            return cupom;
        }catch (RuntimeException ex){
            throw new ResponseStatusException(HttpStatus.BAD_REQUEST, ex.getMessage());
        }
    }
}
