package br.com.controleestoque.services;

import br.com.controleestoque.models.Estoque;
import br.com.controleestoque.models.Produto;
import br.com.controleestoque.repositories.EstoqueRepository;
import br.com.controleestoque.repositories.ProdutoRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.EmptyStackException;
import java.util.List;
import java.util.Optional;

@Service
public class EstoqueService {

    @Autowired
    private EstoqueRepository estoqueRepository;


    @Autowired
    private ProdutoService produtoService;


    public Estoque cadastrarEstoque(Estoque estoque) {
        Estoque estoqueObjeto = estoqueRepository.save(estoque);
        return estoqueObjeto;
    }

    public Estoque cadastrarEstoque(Produto produto, long quantidade) {
        Estoque estoque = new Estoque();
        estoque.setProduto(produto);
        estoque.setQuantidadeEstoque(quantidade);
        return cadastrarEstoque(estoque);
    }

    public long consultarEstoque(int idProduto){

        Produto produto = produtoService.buscarPorId(idProduto);

        Optional<Estoque> optionalEstoque = estoqueRepository.findByProduto(produto);

        if (optionalEstoque.isPresent()) {
            return optionalEstoque.get().getQuantidadeEstoque();
        }
        else {
            throw new RuntimeException("Não há estoque disponivel para o produto selecionado: " + produto.getDescricaoProduto());
        }
    }

    public void reduzirEstoque(List<Integer> listProduto, int quantidade){

        for(int idProduto : listProduto){
            Produto produto = produtoService.buscarPorId(idProduto);

            Optional<Estoque> optionalEstoque = estoqueRepository.findByProduto(produto);

            if (optionalEstoque.isPresent()) {
                optionalEstoque.get().reduzirEstoque(quantidade);
                estoqueRepository.save(optionalEstoque.get());

            }else{
                throw new RuntimeException("Não existe estoque cadastrado para o produto informado");
            }
        }
    }

    public long incrementarEstoque(long quantidadeAtual, long quantidadeIncrementar){
        long quantidadeTotalEstoque = quantidadeAtual + quantidadeIncrementar;
        return quantidadeTotalEstoque;
    }

    public Estoque buscarPorID(Produto produto) {
        Optional<Estoque> optionalEstoque = estoqueRepository.findByProduto(produto);

        if (optionalEstoque.isPresent()) {
            return optionalEstoque.get();
        } else {
            throw new RuntimeException("Não existe estoque cadastrado para o produto informado");
        }
    }

}
