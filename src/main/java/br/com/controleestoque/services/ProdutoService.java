package br.com.controleestoque.services;


import br.com.controleestoque.models.Estoque;
import br.com.controleestoque.models.Produto;
import br.com.controleestoque.models.dtos.ProdutoEntradaDTO;
import br.com.controleestoque.models.dtos.ProdutoRespostaDTO;
import br.com.controleestoque.repositories.ProdutoRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

@Service
public class ProdutoService {

    @Autowired
    private ProdutoRepository produtoRepository;

    @Autowired
    private EstoqueService estoqueService;

    public Produto cadastrarProduto(ProdutoEntradaDTO produtoEntradaDTO) {
        Produto produto = new Produto();
        produto.setValorUnitario(produtoEntradaDTO.getValorUnitario());
        produto.setDescricaoProduto(produtoEntradaDTO.getDescricaoProduto());
        Produto produtoObjeto = produtoRepository.save(produto);

        estoqueService.cadastrarEstoque(produto, produtoEntradaDTO.getQuantidade());

        return produtoObjeto;
    }

    public Produto buscarPorId(int id) {
        Optional<Produto> optionalProduto = produtoRepository.findById(id);
        if (optionalProduto.isPresent()) {
            return optionalProduto.get();
        }
        throw new RuntimeException("O produto não foi encontrado");
    }

    public Iterable<Produto> listarProdutos() {
        Iterable<Produto> produtos = produtoRepository.findAll();
        return produtos;
    }

    public List<Produto> buscarTodosPorID(List<Integer> id){

        Iterable<Produto> produto = produtoRepository.findAllById(id);

        return (List)produto;
    }

    public boolean existeProduto(int id){

        if(produtoRepository.existsById(id))
        {
            return true;
        }
        throw new RuntimeException("O produto não foi encontrado: " + id);
    }

    public Produto atualizarProduto(int id, ProdutoEntradaDTO produtoEntradaDTO){
        if(existeProduto(id)){
            Produto produtoObjeto = buscarPorId(id);
            Estoque estoqueObjeto = estoqueService.buscarPorID(produtoObjeto);

            produtoObjeto.setValorUnitario(produtoEntradaDTO.getValorUnitario());
            produtoObjeto.setDescricaoProduto(produtoEntradaDTO.getDescricaoProduto());
            Produto produto = produtoRepository.save(produtoObjeto);

            estoqueObjeto.setProduto(produtoObjeto);
            estoqueObjeto.setQuantidadeEstoque(estoqueService.incrementarEstoque(estoqueObjeto.getQuantidadeEstoque(), produtoEntradaDTO.getQuantidade()));
            estoqueService.cadastrarEstoque(estoqueObjeto);

            return produtoObjeto;
        }

        throw new RuntimeException("O produto não foi encontrado: " + id);
    }

    public List<ProdutoRespostaDTO> listarProdutosComOuSemEstoque(boolean existeEstoque){
        Iterable<Produto> produtos = listarProdutos();

        List<ProdutoRespostaDTO> produtosRespostaDTOS = new ArrayList<>();

        for(Produto produtoObjeto : produtos){
            Estoque estoqueObjeto = estoqueService.buscarPorID(produtoObjeto);

            if(existeEstoque) {
                if (estoqueObjeto.getQuantidadeEstoque() > 0) {
                    ProdutoRespostaDTO produtoRespostaDTO = new ProdutoRespostaDTO(produtoObjeto.getDescricaoProduto(),
                            produtoObjeto.getValorUnitario(), estoqueObjeto.getQuantidadeEstoque());
                    produtosRespostaDTOS.add(produtoRespostaDTO);
                }
            }
            else
            {
                if (estoqueObjeto.getQuantidadeEstoque() == 0) {
                    ProdutoRespostaDTO produtoRespostaDTO = new ProdutoRespostaDTO(produtoObjeto.getDescricaoProduto(),
                            produtoObjeto.getValorUnitario(), estoqueObjeto.getQuantidadeEstoque());
                    produtosRespostaDTOS.add(produtoRespostaDTO);
                }
            }

        }
        return produtosRespostaDTOS;
    }
}
