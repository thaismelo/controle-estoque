package br.com.controleestoque.services;

import br.com.controleestoque.models.Cliente;
import br.com.controleestoque.repositories.ClienteRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.Optional;

@Service
public class ClienteService {

    @Autowired
    ClienteRepository clienteRepository;

    public Cliente cadastrarCliente(Cliente cliente){
        Cliente novoCliente = clienteRepository.save(cliente);
        return novoCliente;
    }

    public Cliente buscarClientePorEmail(String email){
        Cliente clienteObjeto = clienteRepository.findByEmail(email);
        return clienteObjeto;
    }

    public Cliente buscarClientePorID(int idCliente){
        Optional<Cliente> optionalCliente= clienteRepository.findById(idCliente);
        if(optionalCliente.isPresent()){
            return optionalCliente.get();
        }
        throw  new RuntimeException("Cliente não cadastrado");
    }

}
