package br.com.controleestoque.services;

import br.com.controleestoque.enums.TipoPagamentoEnum;
import br.com.controleestoque.models.Cliente;
import br.com.controleestoque.models.Cupom;
import br.com.controleestoque.models.Pedido;
import br.com.controleestoque.models.Produto;
import br.com.controleestoque.models.dtos.PedidoClienteRespostaDTO;
import br.com.controleestoque.models.dtos.PedidoRespostaDTO;
import br.com.controleestoque.repositories.PedidoRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.swing.text.html.Option;
import java.math.BigDecimal;
import java.math.RoundingMode;
import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

@Service
public class PedidoService {

    @Autowired
    private PedidoRepository pedidoRepository;

   @Autowired
    private ProdutoService produtoService;

    @Autowired
    private EstoqueService estoqueService;

    @Autowired
    private ClienteService clienteService;

    @Autowired
    private  CupomService cupomService;

    public PedidoRespostaDTO registrarCompra(Pedido pedido){
            Cliente clienteObjeto = clienteService.buscarClientePorID(pedido.getCliente().getId());

            if (clienteObjeto != null) {
                pedido.setCliente(clienteObjeto);

                if(pedido.getCupom() != null) {
                    pedido.setCupom(cupomService.buscarPorID(pedido.getCupom().getId()));
                }

                List<Integer> idProdutos = new ArrayList<>();
                String produtosSemEstoque = "";

                for (Produto produto : pedido.getProdutos()) {
                    if(produtoService.existeProduto(produto.getId())) {
                        if(estoqueService.consultarEstoque(produto.getId()) > 0) {
                            idProdutos.add(produto.getId());
                        }
                        else{
                            produtosSemEstoque += produtoService.buscarPorId(produto.getId()).getDescricaoProduto() + ", ";
                        }
                    }
                    else{
                        throw  new RuntimeException("Produto não cadastrado");
                    }
                }

                if(produtosSemEstoque.isEmpty()){
                    List<Produto> produtoObjeto = produtoService.buscarTodosPorID(idProdutos);
                    pedido.setProdutos(produtoObjeto);
                    estoqueService.reduzirEstoque(idProdutos, 1);

                }else{
                    throw  new RuntimeException("Não há estoque disponivel para o produto selecionado: " + produtosSemEstoque.substring(0, produtosSemEstoque.length()-2));
                }

                pedido.setValorTotal(calcularValorTotal(pedido.getCupom(), pedido.getProdutos()));
                pedido.setDataDaCompra(LocalDate.now());

                if(pedido.getCupom() != null) {
                    pedido.setCupom(cupomService.buscarPorID(pedido.getCupom().getId()));
                }

                pedidoRepository.save(pedido);

            }else{

                throw  new RuntimeException("Cliente não cadastrado");
            }

            PedidoRespostaDTO pedidoRespostaDTO = new PedidoRespostaDTO(pedido.getValorTotal(),
                    pedido.getProdutos().size(), pedido.getTipoPagamento(), pedido.getDataDaCompra());

            return pedidoRespostaDTO;
    }

    private double calcularValorTotal(Cupom cupom, List<Produto> produtos){
        double valorTotalDaCompra = 0;

        for(Produto produto : produtos){
            valorTotalDaCompra += produto.getValorUnitario();
        }


        if(cupom != null){
            Cupom cupomObjeto = cupomService.VerificarCupomDisponivel(cupom.getId());
            valorTotalDaCompra -= (valorTotalDaCompra * cupomObjeto.getPorcentagemDesconto()) / 100;
        }

        BigDecimal bd = new BigDecimal(valorTotalDaCompra).setScale(3, RoundingMode.FLOOR);
        valorTotalDaCompra = bd.doubleValue();
        return valorTotalDaCompra;

    }

    public List<PedidoRespostaDTO> listarValorTotalDoDia(LocalDate data){
        Iterable<Pedido> pedidos =  pedidoRepository.findAllByDataDaCompra(data);

        List<PedidoRespostaDTO> listaPedidoRespostaDTO = new ArrayList<PedidoRespostaDTO>();

        for(Pedido pedidoObjeto : pedidos){
            PedidoRespostaDTO pedidoRespostaDTO =  new PedidoRespostaDTO(pedidoObjeto.getValorTotal(),
                    pedidoObjeto.getProdutos().size(), pedidoObjeto.getTipoPagamento(), pedidoObjeto.getDataDaCompra());
            listaPedidoRespostaDTO.add(pedidoRespostaDTO);
        }
        return listaPedidoRespostaDTO;
    }

    public List<PedidoRespostaDTO> listarValorTotal(){
        Iterable<Pedido> pedidos =  pedidoRepository.findAll();

        List<PedidoRespostaDTO> listaPedidoRespostaDTO = new ArrayList<PedidoRespostaDTO>();

        for(Pedido pedidoObjeto : pedidos){
            PedidoRespostaDTO pedidoRespostaDTO =  new PedidoRespostaDTO(pedidoObjeto.getValorTotal(),
                    pedidoObjeto.getProdutos().size(), pedidoObjeto.getTipoPagamento(), pedidoObjeto.getDataDaCompra());
            listaPedidoRespostaDTO.add(pedidoRespostaDTO);
        }
        return listaPedidoRespostaDTO;
    }

    public List<PedidoRespostaDTO> listarPorTipoPagamento(TipoPagamentoEnum tipoPagamentoEnum){
        Iterable<Pedido> pedidos =  pedidoRepository.findAllByTipoPagamento(tipoPagamentoEnum);

        List<PedidoRespostaDTO> listaPedidoRespostaDTO = new ArrayList<PedidoRespostaDTO>();

        for(Pedido pedidoObjeto : pedidos){
            PedidoRespostaDTO pedidoRespostaDTO =  new PedidoRespostaDTO(pedidoObjeto.getValorTotal(),
                    pedidoObjeto.getProdutos().size(), pedidoObjeto.getTipoPagamento(), pedidoObjeto.getDataDaCompra());
            listaPedidoRespostaDTO.add(pedidoRespostaDTO);
        }
        return listaPedidoRespostaDTO;
    }

    public List<PedidoClienteRespostaDTO> listarPedidoPorCliente(int idCliente) {

        List<PedidoClienteRespostaDTO> listaPedidoClienteRespostaDTO = new ArrayList<PedidoClienteRespostaDTO>();


        Cliente clienteObjeto = clienteService.buscarClientePorID(idCliente);

        Iterable<Pedido> pedidos = pedidoRepository.findAllByCliente(clienteObjeto);


        for (Pedido pedido : pedidos) {
            for (Produto produto : pedido.getProdutos()) {
                PedidoClienteRespostaDTO pedidoClienteRespostaDTO = new PedidoClienteRespostaDTO(clienteObjeto.getNome(),
                        pedido.getDataDaCompra(), produto.getDescricaoProduto(), pedido.getValorTotal());

                listaPedidoClienteRespostaDTO.add(pedidoClienteRespostaDTO);
            }
        }

        return listaPedidoClienteRespostaDTO;
    }
}
