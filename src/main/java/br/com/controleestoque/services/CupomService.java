package br.com.controleestoque.services;

import br.com.controleestoque.models.Cupom;
import br.com.controleestoque.repositories.CupomRepository;
import org.apache.tomcat.jni.Local;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.format.annotation.DateTimeFormat;
import org.springframework.stereotype.Service;

import javax.swing.text.html.Option;
import java.text.DateFormat;
import java.time.LocalDate;
import java.time.ZoneId;
import java.util.Calendar;
import java.util.Date;
import java.util.Optional;

@Service
public class CupomService {

    @Autowired
    CupomRepository cupomRepository;

    public Cupom cadastrarCupom(Cupom cupom)
    {
        if(VerificarCupomDisponivel(cupom)) {
            Cupom cupomObjeto = cupomRepository.save(cupom);
            return cupomObjeto;
        }
        else
            throw new RuntimeException("Data inválida");

    }

    public boolean VerificarCupomDisponivel(Cupom cupom)
    {
        if(cupom.getDataValidade().isBefore(LocalDate.now()))
        {
            throw new RuntimeException("Cupom está vencido");
        }
        else
            return true;
    }

    public Iterable<Cupom> buscarCupons()
    {
        Iterable<Cupom> cupoms = cupomRepository.findAll();
        return cupoms;
    }

    public void deleteCupom(int id)
    {
        if (cupomRepository.existsById(id))
        {
            cupomRepository.deleteById(id);
        }
        else
            throw new RuntimeException("Id não existe");
    }

    public Cupom VerificarCupomDisponivel(int idCupom)
    {
        Cupom cupom = buscarPorID(idCupom);
        if(cupom != null) {
            if (cupom.getDataValidade().isBefore(LocalDate.now())) {
                throw new RuntimeException("Cupom está vencido");
            }
            return cupom;
        }
       else
            throw new RuntimeException("Cupom invaĺido");
    }

    public Cupom buscarPorID(int id) {
        Optional<Cupom> optionalCupom = cupomRepository.findById(id);
        if (optionalCupom.isPresent()) {
            return optionalCupom.get();
        } else {
            return null;
        }
    }

}
