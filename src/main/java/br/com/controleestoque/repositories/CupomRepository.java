package br.com.controleestoque.repositories;

import br.com.controleestoque.models.Cupom;
import org.springframework.data.repository.CrudRepository;

public interface CupomRepository extends CrudRepository<Cupom,Integer> {
}
