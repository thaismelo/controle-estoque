package br.com.controleestoque.repositories;

import br.com.controleestoque.models.Cliente;
import org.springframework.data.repository.CrudRepository;

public interface ClienteRepository extends CrudRepository<Cliente,Integer> {

    Cliente findByEmail(String email);
}
