package br.com.controleestoque.repositories;

import br.com.controleestoque.enums.TipoPagamentoEnum;
import br.com.controleestoque.models.Cliente;
import br.com.controleestoque.models.Pedido;
import org.springframework.data.repository.CrudRepository;

import java.time.LocalDate;

public interface PedidoRepository extends CrudRepository<Pedido,Integer> {
    Iterable<Pedido> findAllByDataDaCompra(LocalDate data);
    Iterable<Pedido> findAllByTipoPagamento(TipoPagamentoEnum tipoPagamentoEnum);
    Iterable<Pedido> findAllByCliente(Cliente cliente);
}
