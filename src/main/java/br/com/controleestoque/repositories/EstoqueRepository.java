package br.com.controleestoque.repositories;

import br.com.controleestoque.models.Estoque;
import br.com.controleestoque.models.Produto;
import org.springframework.data.repository.CrudRepository;

import java.util.Optional;

public interface EstoqueRepository extends CrudRepository<Estoque,Integer> {
    Optional<Estoque>  findByProduto(Produto produto);
}
