package br.com.controleestoque.repositories;

import br.com.controleestoque.models.Produto;
import org.springframework.data.repository.CrudRepository;

public interface ProdutoRepository extends CrudRepository<Produto,Integer> {
}
