package br.com.controleestoque.repositories;

import br.com.controleestoque.models.Usuario;
import org.springframework.data.repository.CrudRepository;

public interface UsuarioRepository extends CrudRepository<Usuario,Integer> {
    Usuario findByEmail(String email);
}
