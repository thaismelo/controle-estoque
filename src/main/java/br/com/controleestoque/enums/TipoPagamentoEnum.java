package br.com.controleestoque.enums;

public enum TipoPagamentoEnum {
    CARTAOCREDITO,
    CARTAODEBITO,
    DINHEIRO
}
